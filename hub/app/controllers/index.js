import Ember from 'ember';
import rulesArrayProxy from '../helpers/rulesArrayProxy';
import rule from '../models/rule';
import result from '../models/result';

export default Ember.Controller.extend({
    model:rulesArrayProxy.create({ content: Ember.A([]) }),
    flowParam:"//Object passed as param to the rule function\n{\n\tcolor:'red'\n}",
    executionResult:Ember.A([]),
    init:function(){
        this.get('model').set('content',Ember.A([
            rule.create({title: 'Rule 1', body: "function(obj){\n\tif(obj.color == 'red')\n\t\treturn true;\n\telse\n\t\treturn false;\n}", passId: '2', failId: '3'}),
            rule.create({title: 'Rule 2', body: "function(obj){\n\tif(obj.color == 'blue')\n\t\treturn true;\n\telse\n\t\treturn false;\n}", passId: '4', failId: '5'}),
            rule.create({title: 'Rule 3', body: "function(obj){\n\treturn !obj;\n}", passId: '', failId: ''}),
            rule.create({title: 'Rule 4', body: "function(obj){\n\treturn false;\n}", passId: '', failId: ''}),
            rule.create({title: 'Rule 5', body: "function(obj){\n\treturn true;\n}", passId: '', failId: ''})
        ]));
    },
    /**
     * show error
     *
     * @param error
     */
    showError:function(error){
        alert(error);
    },
    /**
     * The method contains try catch because the user can write
     * broken code so we make sure the app dont crash in this case and
     * show him what he wrote wrong
     *
     * @param id
     * @returns {*}
     */
    executeSingleRule:function(id){
        try{
            var rule = this.get('model.content').objectAt(id-1);
            var userRule = "var userFunc = "+rule.get('body');
            var result = eval(userRule+";userFunc("+this.get('flowParam')+")");

            return {title:rule.get('title'), passId:rule.get('passId'), failId:rule.get('failId'), state:result};
        }catch (e){
            this.showError("Error ( rule Id "+id+" )   "+e.message);
            return {title:"", passId:"", failId:"", state:false};
        }
    },
    addResult:function(resultOfRule){
        this.get('executionResult').pushObject(result.create(resultOfRule));
    },
    actions:{
        /**
         * Execute the engine Flow
         */
        executeFlow:function(){
            this.set('executionResult',Ember.A([]));
            var error = this.get('model').hasErrors();
            if(!error){
                var rule = 1;
                while(true){
                    var result = this.executeSingleRule(rule);
                    this.addResult(result);

                    if(result.passId === "" || result.failId === ""){
                        break;
                    }

                    if(result.state){
                        rule = result.passId;
                    }else{
                        rule = result.failId;
                    }
                }
            }else{
                this.showError(error);
            }
        },
        /**
         * add new rule
         */
        addNewRule:function(){
            this.get('model.content').pushObject(rule.create({title:"New Rule"}));
        },
        newBlankFlow:function(){
            this.set('executionResult',Ember.A([]));
            this.set('model.content',Ember.A([]));
            this.set('flowParam',"");
        }
    }
});
