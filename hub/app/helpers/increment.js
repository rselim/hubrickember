import Ember from 'ember';

export default Ember.Handlebars.helper('increment',function(params) {
    return params + 1;
});

