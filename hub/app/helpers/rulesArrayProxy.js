import Ember from 'ember';

export default Ember.ArrayProxy.extend({
    content:null,
    /**
     * Check if given array has unique values
     * so we make sure that pass id / fail id doesnt repeats on two different rules
     *
     * @param array
     * @returns {boolean}
     */
    isArrayIsUnique:function(array){
        array = array.sort();
        for ( var i = 1; i < array.length; i++ ){
            if(array[i-1] === array[i]) {
                return false; //duplicate value
            }
        }
        return true;
    },
    hasErrors:function(){
        var title,body,pass,fail,ids;
        ids = [];
        if(this.get('content').length < 1){
            return "Error there is no rules.. add some.";
        }
        //check that title and body have input on all rules
        for(var i = 0;i<this.get('content').length;i++){
            var rule = this.get('content').objectAt(i);
            title = rule.get('title');
            body = rule.get('body');
            pass = rule.get('passId');
            fail = rule.get('failId');

            if(title.trim() === "") {
                return "Error ( rule id " + (i + 1) + " ) title cant be blank";
            }
            if(body.trim() === "") {
                return "Error ( rule id " + (i + 1) + " ) body cant be blank";
            }
            if(pass === 1|| fail === 1) {
                return "Error ( rule id " + (i + 1) + " ) first rule id cant be in pass/fail id";
            }

            if(pass.trim() !== "") {
                ids.push(pass.trim());
            }
            if(fail.trim() !== "") {
                ids.push(fail.trim());
            }
        }

        if(this.isArrayIsUnique(ids)) {
            return false; // No errors
        }
        else {
            return "Error it seems that one of the rule ids is duplicated with different pass_id / fail_id";
        }
    }
});