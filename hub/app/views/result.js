import Ember from 'ember';

export default Ember.View.extend({
    classNames:['hexagon','fadeIn'],
    classNameBindings:['result.state:valid:error'],
    templateName: "result",
    result:null
});
