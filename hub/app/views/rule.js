import Ember from 'ember';
import increment from '../helpers/increment';

export default Ember.View.extend({
    templateName: "rule",
    rule:null,
    classNameBindings:['showCodeBlock:code-ready'],
    showCodeBlock:true,
    codeHasLines:false,
    didInsertElement:function(){
        var _this = this;
        _this.$().delegate('pre','click',function(){
            _this.toggleProperty('showCodeBlock');
            var $textarea = _this.$().find('textarea:first');
            Ember.run.next(function(){
                $textarea.focus();
            });

        });
        _this.$().delegate('textarea','blur',function(){
            _this.toggleProperty('showCodeBlock');
        });

    }
});
