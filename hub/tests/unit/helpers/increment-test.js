import { increment } from '../../../helpers/increment';
import { module, test } from 'qunit';

module('Unit | Helper | increment');

// Replace this with your real tests.
test('it works', function(assert) {
  var result = increment(42);
  assert.ok(result);
});
